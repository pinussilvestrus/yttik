package com.Kiefer.YttikJava;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.Kiefer.YttikJava.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements OnTouchListener{ //implements OnTouchListener
	
	ImageView img1;
	ImageView img2;
	ImageView img3;
	ImageView img4;

	ImageView imgEvil;
	ImageView imgEvil2;

	ImageView imgGold;
	ImageView imgRed;

	ImageView imgStart;
	ImageView imgClose;
	ImageView imgGameOver;

	TextView txtv1;
	TextView txtv2;
	TextView txtv3;
	Random machine;
	RelativeLayout lay;
	RelativeLayout fray;
	
	Timer myTimer;
	TimerTask timertsk;
	int count;
	int pointcount;
	int counterStart;

	float width;
	float height;

	private SharedPreferences prefs; //Highscore
	private SharedPreferences.Editor editor;
	public int highscore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        count = 0;
		pointcount = 0;
		counterStart = 0;
		
		img1 = (ImageView)findViewById(R.id.imageView1);
		img1.setVisibility(View.INVISIBLE);

		img2 = (ImageView)findViewById(R.id.imageView2);
		img2.setVisibility(View.INVISIBLE);

		img3 = (ImageView)findViewById(R.id.imageView3);
		img3.setVisibility(View.INVISIBLE);

		img4 = (ImageView)findViewById(R.id.imageView5);
		img4.setVisibility(View.INVISIBLE);

		imgEvil = (ImageView)findViewById(R.id.imageView4);
		imgEvil.setVisibility(View.INVISIBLE);

		imgEvil2 = (ImageView)findViewById(R.id.imageView6);
		imgEvil2.setVisibility(View.INVISIBLE);

		imgGold = (ImageView)findViewById(R.id.imageGold);
		imgGold.setVisibility(View.INVISIBLE);

		imgRed = (ImageView)findViewById(R.id.imageRed);
		imgRed.setVisibility(View.INVISIBLE);
		
		txtv1 = (TextView) findViewById (R.id.textView1);
		txtv1.setShadowLayer (1, 2, 2, Color.BLACK);

		txtv2 = (TextView) findViewById (R.id.textView2);
		lay = (RelativeLayout) findViewById (R.id.linear1);
		
		width = lay.getWidth();
		height = lay.getHeight();
		
		prefs = getSharedPreferences ("Highscore", Context.MODE_WORLD_READABLE);
		editor = prefs.edit ();
		getHighscore ();
		
		//Frame
		fray = (RelativeLayout) findViewById (R.id.frameLayout1);
		txtv3 = (TextView) findViewById (R.id.textView3);
		txtv3.setShadowLayer (1, 2, 2, Color.BLACK);
		imgStart = (ImageView) findViewById (R.id.NeustartButton);
		imgClose = (ImageView) findViewById (R.id.SchliessenButton);
		imgGameOver = (ImageView) findViewById (R.id.imageView7);
		
		//Timer
		myTimer = new Timer();
		myTimer.schedule(new TimerTask() {          
			@Override
			public void run() {
				TimerMethod();
			}
		//interval 3.parameter (1000=1s)
		}, 1000, 1200);
		
		img1.setOnTouchListener (this);
		img2.setOnTouchListener (this);
		img3.setOnTouchListener (this);
		img4.setOnTouchListener (this);
		imgEvil.setOnTouchListener (this);
		imgEvil2.setOnTouchListener (this);
		imgGold.setOnTouchListener (this);
		imgRed.setOnTouchListener (this);
		
		imgStart.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	Intent in = new Intent(MainActivity.this,MainActivity.class);
            	startActivity(in);
    			MainActivity
            }
        });

		};

		imgClose.Click += delegate {

			StartActivity (typeof(Startseite));
			this.Finish ();
		};
	}
    
    private void TimerMethod()
	{
		//This method is called directly by the timer
		//and runs in the same thread as the timer.

		//We call the method that will work with the UI
		//through the runOnUiThread method.
		this.runOnUiThread(Timer_Tick);
	}


	private Runnable Timer_Tick = new Runnable() {
		public void run() {

			//This method runs in the same thread as the UI.               

			//Do something to the UI thread here
			
			if (counterStart==0)
			{
				img1.setX(30000);
				img1.setY(10000);
				img2.setX(30000);
				img2.setY(10000);
				img3.setX(30000);
				img3.setY(10000);
				img4.setX(30000);
				img4.setY(10000);
				imgEvil2.setX(30000);
				imgEvil2.setY(10000);
				imgEvil.setX(30000);
				imgEvil.setY(10000);

				imgGold.setX(30000);
				imgGold.setY(10000);
				imgRed.setX(30000);
				imgRed.setY(10000);

				counterStart++;
			}

			IMGChange(img1);
			IMGChange(img2);
			IMGChange(img3);
			IMGChange(img4);
			IMGChange(imgEvil);
			IMGChange(imgEvil2);
			IMGChange(imgGold);
			IMGChange(imgRed);

			IMGAppear(img1);
			IMGAppear(img2);
			IMGAppear(img3);
			IMGAppear(imgEvil);
			IMGAppear(img4);
			IMGAppear(imgEvil2);
			IMGAppear(imgGold);
			IMGAppear(imgRed);
		}
	};
	
	
	private void IMGChange(View v) //Positionen eines Bildes
	{
		machine = new Random ();

		float theX = v.getX();
		float theY = v.getY();

		width = lay.getWidth();
		height = lay.getHeight();

		int widthSet = (int) width / 4;
		int heightSet = (int) height / 4;

		v.layout (0, 0, widthSet, heightSet);

		float number = (float)machine.nextInt(80);
		float number2 = (float)machine.nextInt(80);
		number = (number + number2) / 2;
		number = number / 100;
		width = width * number;

		number = (float)machine.nextInt(80);
		number2 = (float)machine.nextInt(80);


		number = (number + number2) / 2;
		number = number / 100;
		height = height * number;


		theX = width;
		theY = height;

		v.setY (theY);
		v.setX (theX);
	}
	
	private void IMGAppear(View v) //unterschiedliches Erscheinen aller Bilder
	{

		int number = machine.nextInt (3)+1;
		if (number <= 2) {

			v.setVisibility(View.VISIBLE);;
			if (v.equals(imgGold) || v.equals(imgRed)) {

				number = machine.nextInt (3)+1;
				if (number <= 2) {
					v.setVisibility(View.INVISIBLE);

				}

			}

		} else {
			v.setVisibility(View.INVISIBLE);;
		}
	}
	
	public boolean onTouch(View v, MotionEvent e)
	{
		switch (e.getAction()) {
		
		case MotionEvent.ACTION_DOWN:
			if (v.equals (imgEvil) || v.equals (imgEvil2)) {
				GameOver ();
			} else if (v.equals (imgGold)) {
				pointcount += 10;
				v.setVisibility(View.INVISIBLE);
				txtv1.setText(Integer.toString(pointcount)); 
				//pause();
				IMGChange (v);
				//start();
			} else if (v.equals (imgRed)) {
				pointcount--;
				pointcount--;
				pointcount--;
				v.setVisibility(View.INVISIBLE);
				txtv1.setText(Integer.toString(pointcount));
				//pause();
				IMGChange (v);
				//start();

			}

			else {
				pointcount++;
				v.setVisibility(View.INVISIBLE);
				txtv1.setText(Integer.toString(pointcount));
				//pause();
				IMGChange (v);
				//start();
			}	
		
		}
		
			
			return true;
			
		}

	public int getHighscore()
	{
		if (prefs.contains ("Highscore")) {
			highscore = Integer.parseInt(prefs.getString ("Highscore", ""));
		} else {
			editor.putString ("Highscore", "0");
			editor.commit ();
			highscore = Integer.parseInt(prefs.getString ("Highscore", ""));

		}

		txtv2.setText(Integer.toString(highscore));
		return highscore;
	}
	
	private void GameOver()
	{
		myTimer.cancel();


		// show an alert
		if (pointcount > getHighscore()) {

			//new
			fray.setVisibility(View.VISIBLE);
			lay.setClickable(false);
			img1.setVisibility(View.INVISIBLE);
			img2.setVisibility(View.INVISIBLE);
			img3.setVisibility(View.INVISIBLE);
			img4.setVisibility(View.INVISIBLE);
			imgEvil.setVisibility(View.INVISIBLE);
			imgEvil2.setVisibility(View.INVISIBLE);
			imgGold.setVisibility(View.INVISIBLE);
			imgRed.setVisibility(View.INVISIBLE);
			imgGameOver.setImageResource (R.drawable.gameoverhigh);
			txtv3.setText(txtv1.getText());
			editor.putString ("Highscore", Integer.toString(pointcount));
			editor.commit ();

			getHighscore ();
		} else {

			fray.setVisibility(View.VISIBLE);
			lay.setClickable(false);
			img1.setVisibility(View.INVISIBLE);
			img2.setVisibility(View.INVISIBLE);
			img3.setVisibility(View.INVISIBLE);
			img4.setVisibility(View.INVISIBLE);
			imgEvil.setVisibility(View.INVISIBLE);
			imgEvil2.setVisibility(View.INVISIBLE);
			imgGold.setVisibility(View.INVISIBLE);
			imgRed.setVisibility(View.INVISIBLE);
			imgGameOver.setImageResource (R.drawable.gameovernormal);
			txtv3.setText(txtv1.getText());
		}
	}

}
